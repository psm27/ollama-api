<?php
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');

function sendEvent($data) {
    echo "data: $data\n\n";
    ob_flush();
    flush();
}

// Get the query parameter from GET request
$query = isset($_GET['q']) ? $_GET['q'] : 'Who made Rose promise that she would never let go?';

// API endpoint URL
$url = 'http://localhost:11434/api/generate';

// Data to be sent in the POST request
$data = array(
    'model' => 'openchat',
    'prompt' => $query
);

// Convert the data array to JSON format
$data_json = json_encode($data);

// cURL initialization
$ch = curl_init($url);

// Set cURL options
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: ' . strlen($data_json)
));

// Function to handle the response in real-time
curl_setopt($ch, CURLOPT_WRITEFUNCTION, function ($ch, $str) {
    // Decode the JSON string
    $json = json_decode($str, true);

    // Check if the "response" field exists
    if (isset($json['response'])) {
        // Send the "response" text as an SSE event
        sendEvent($json['response']);
    }

    return strlen($str);
});
// Execute cURL session
curl_exec($ch);

// Check for cURL errors
if (curl_errno($ch)) {
    sendEvent('Curl error: ' . curl_error($ch));
}

// Close cURL session
curl_close($ch);
