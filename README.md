# Ollama-api

# Installing Ollama

```shell
curl https://ollama.ai/install.sh | sh
```
```
sudo apt install php-curl
```

## Getting started

```
ollama run openchat
```

## Changing the model

Go to `event.php` and change "openchat" with new model name
